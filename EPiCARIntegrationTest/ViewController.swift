//
//  ViewController.swift
//  EPiCARIntegrationTest
//
//  Created by Marco Pagliari on 28/01/2020.
//  Copyright © 2020 Marco Pagliari. All rights reserved.
//

import UIKit
import EpicAR

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }

    @IBAction func navigate(_ sender: Any) {
        let target = EARViewController()
        navigationController?.pushViewController(target, animated: true)
    }

    @IBAction func presentFullscreen(_ sender: Any) {
        let target = EARViewController()
        target.view.backgroundColor = UIColor.black

        target.modalPresentationStyle = .fullScreen
        target.view.backgroundColor = UIColor.black

        present(target, animated: true)
    }

    @IBAction func presentFullscreenInNavigation(_ sender: Any) {
        let navigation = UINavigationController()
        let target = EARViewController()
        target.view.backgroundColor = UIColor.black

        let closeDialogButton = UIBarButtonItem(barButtonSystemItem: .stop, target: self, action: #selector(target.dismissDialog))

        navigation.modalPresentationStyle = .fullScreen
        navigation.addChild(target)
        _ = target.addLeftBarButton(closeDialogButton)
        present(navigation, animated: true)
    }

    @IBAction func presentSheet(_ sender: Any) {
        let target = EARViewController()
        target.view.backgroundColor = UIColor.black

        if #available(iOS 13.0, *) {
            target.modalPresentationStyle = .automatic
        } else {
            target.modalPresentationStyle = .pageSheet
        }

        target.view.backgroundColor = UIColor.black

        present(target, animated: true)
    }

    @IBAction func presentSheetInNavigation(_ sender: Any) {
        let navigation = UINavigationController()
        let target = EARViewController()
        target.view.backgroundColor = UIColor.black

        let closeDialogButton = UIBarButtonItem(barButtonSystemItem: .stop, target: self, action: #selector(target.dismissDialog))

        if #available(iOS 13.0, *) {
            navigation.modalPresentationStyle = .automatic
        } else {
            navigation.modalPresentationStyle = .pageSheet
        }

        navigation.addChild(target)
        _ = target.addLeftBarButton(closeDialogButton)
        present(navigation, animated: true)
    }
}

extension UIViewController {

    @IBAction
    func dismissDialog() {
        self.dismiss(animated: true, completion: nil)
    }

    func addLeftBarButton(_ button: UIBarButtonItem) -> Bool {
        if self.navigationItem.leftBarButtonItems == nil {
            self.navigationItem.leftBarButtonItems = []
        }

        if let items = navigationItem.leftBarButtonItems,
            !items.contains(button) {
            self.navigationItem.leftBarButtonItems?.insert(button, at: 0)
            return true
        } else {
            return false
        }
    }
}



