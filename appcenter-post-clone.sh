#!/usr/bin/env bash
echo "post-clone script"

env

ssh-add -L

# Encoded file generated following the guide: https://www.shellhacks.com/encrypt-decrypt-file-password-openssl/
openssl enc -aes-256-cbc -d -in appcenter_rsa.enc -out appcenter_rsa -k $AZURE_RSA_DECODE_PASS
cat appcenter_rsa | tr -d '\r' | ssh-add -
ssh-add -L
rm appcenter_rsa

pod --version --verbose
pod repo --verbose
pod repo update --verbose
pod install --verbose

